package at.a1.kafkastream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaStream {

  public static void main(String[] args) {
    SpringApplication.run(KafkaStream.class, args);
  }

}
