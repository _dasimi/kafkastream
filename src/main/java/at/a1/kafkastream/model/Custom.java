package at.a1.kafkastream.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Custom {

  @Id
  @GeneratedValue
  private int customId;
  private String eventId;
  private String concept;
  private long eventTime;
  private String event;
  private String linkedSessionId;
  private String eventSource;

  protected Custom() {
  }

  public Custom(String concept, long eventTime, String eventId, String event, String linkedSessionId, String eventSource) {
    this.concept = concept;
    this.eventTime = eventTime;
    this.eventId = eventId;
    this.event = event;
    this.linkedSessionId = linkedSessionId;
    this.eventSource = eventSource;
  }

  public String getConcept() {
    return concept;
  }

  public void setConcept(String concept) {
    this.concept = concept;
  }

  public long getEventTime() {
    return eventTime;
  }

  public void setEventTime(long eventTime) {
    this.eventTime = eventTime;
  }

  public String getEventId() {
    return eventId;
  }

  public void setEventId(String eventId) {
    this.eventId = eventId;
  }

  public String getEvent() {
    return event;
  }

  public void setEvent(String event) {
    this.event = event;
  }

  public String getLinkedSessionId() {
    return linkedSessionId;
  }

  public void setLinkedSessionId(String linkedSessionId) {
    this.linkedSessionId = linkedSessionId;
  }

  public String getEventSource() {
    return eventSource;
  }

  public void setEventSource(String eventSource) {
    this.eventSource = eventSource;
  }

}
