package at.a1.kafkastream.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class ViewDataSummary {

  @Id
  @GeneratedValue
  private int viewDataSummaryId;

  private int framedrops;
  @JsonProperty("kiloBytesDownloaded")
  private int kilobytesDownloaded;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "viewDataId")
  private ViewData viewData;

  public ViewDataSummary(int framedrops, int kilobytesDownloaded) {
    this.framedrops = framedrops;
    this.kilobytesDownloaded = kilobytesDownloaded;
  }

  protected ViewDataSummary() {
  }

  public int getViewDataSummaryId() {
    return viewDataSummaryId;
  }

  public void setViewDataSummaryId(int viewDataSummaryId) {
    this.viewDataSummaryId = viewDataSummaryId;
  }

  public int getFramedrops() {
    return framedrops;
  }

  public void setFramedrops(int framedrops) {
    this.framedrops = framedrops;
  }

  public int getKilobytesDownloaded() {
    return kilobytesDownloaded;
  }

  public void setKilobytesDownloaded(int kilobytesDownloaded) {
    this.kilobytesDownloaded = kilobytesDownloaded;
  }

}
