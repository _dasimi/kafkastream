package at.a1.kafkastream.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User {

  @Id
  private String userId;
  private String userType;

  public User(String userId, String userType) {
    this.userId = userId;
    this.userType = userType;
  }

  protected User() {
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserType() {
    return userType;
  }

  public void setUserType(String userType) {
    this.userType = userType;
  }

}
