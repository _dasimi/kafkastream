package at.a1.kafkastream.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class ViewData {

  @Id
  @GeneratedValue
  private int viewDataId;

  private String deliveryProtocol;
  private String service;

  @OneToOne(mappedBy = "viewData", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  private ContentRef contentRef;

  @JsonProperty("summary")
  @OneToOne(mappedBy = "viewData", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  private ViewDataSummary viewDataSummary;

  public ViewData(String deliveryProtocol, String service, ContentRef contentRef, ViewDataSummary viewDataSummary) {
    this.deliveryProtocol = deliveryProtocol;
    this.service = service;
    this.contentRef = contentRef;
    this.viewDataSummary = viewDataSummary;
  }

  protected ViewData() {
  }

  public int getViewDataId() {
    return viewDataId;
  }

  public void setViewDataId(int viewDataId) {
    this.viewDataId = viewDataId;
  }

  public String getDeliveryProtocol() {
    return deliveryProtocol;
  }

  public void setDeliveryProtocol(String deliveryProtocol) {
    this.deliveryProtocol = deliveryProtocol;
  }

  public String getService() {
    return service;
  }

  public void setService(String service) {
    this.service = service;
  }

  public ContentRef getContentRef() {
    return contentRef;
  }

  public void setContentRef(ContentRef contentRef) {
    this.contentRef = contentRef;
  }

  public ViewDataSummary getViewDataSummary() {
    return viewDataSummary;
  }

  public void setViewDataSummary(ViewDataSummary viewDataSummary) {
    this.viewDataSummary = viewDataSummary;
  }

}
