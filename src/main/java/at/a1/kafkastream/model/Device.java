package at.a1.kafkastream.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Device {

  @Id
  private String deviceId;

  private String applicationName;
  private String applicationProvider;
  private String applicationVersion;
  private String contextName;
  private String contextVersion;
  private String deviceControl;
  private String deviceModel;
  private String deviceType;
  private String configurationName;

  public Device(String deviceId, String applicationName, String applicationProvider, String applicationVersion, String contextName, String contextVersion, String deviceControl, String deviceModel, String deviceType, List<ApplicationData> applicationData, String configurationName) {
    this.deviceId = deviceId;
    this.applicationName = applicationName;
    this.applicationProvider = applicationProvider;
    this.applicationVersion = applicationVersion;
    this.contextName = contextName;
    this.contextVersion = contextVersion;
    this.deviceControl = deviceControl;
    this.deviceModel = deviceModel;
    this.deviceType = deviceType;
    this.configurationName = configurationName;
  }

  protected Device() {
  }

  public String getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  public String getApplicationName() {
    return applicationName;
  }

  public void setApplicationName(String applicationName) {
    this.applicationName = applicationName;
  }

  public String getApplicationProvider() {
    return applicationProvider;
  }

  public void setApplicationProvider(String applicationProvider) {
    this.applicationProvider = applicationProvider;
  }

  public String getApplicationVersion() {
    return applicationVersion;
  }

  public void setApplicationVersion(String applicationVersion) {
    this.applicationVersion = applicationVersion;
  }

  public String getContextName() {
    return contextName;
  }

  public void setContextName(String contextName) {
    this.contextName = contextName;
  }

  public String getContextVersion() {
    return contextVersion;
  }

  public void setContextVersion(String contextVersion) {
    this.contextVersion = contextVersion;
  }

  public String getDeviceControl() {
    return deviceControl;
  }

  public void setDeviceControl(String deviceControl) {
    this.deviceControl = deviceControl;
  }

  public String getDeviceModel() {
    return deviceModel;
  }

  public void setDeviceModel(String deviceModel) {
    this.deviceModel = deviceModel;
  }

  public String getDeviceType() {
    return deviceType;
  }

  public void setDeviceType(String deviceType) {
    this.deviceType = deviceType;
  }

  public String getConfigurationName() {
    return configurationName;
  }

  public void setConfigurationName(String configurationName) {
    this.configurationName = configurationName;
  }

}
