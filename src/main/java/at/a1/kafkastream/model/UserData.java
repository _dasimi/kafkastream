package at.a1.kafkastream.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class UserData {

  @Id
  @GeneratedValue
  private int userDataId;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "householdId")
  private Household household;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "profileId")
  private Profile profile;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "userId")
  private User user;

  public UserData(Household household, Profile profile, User user) {
    this.household = household;
    this.profile = profile;
    this.user = user;
  }

  protected UserData() {
  }

  public Profile getProfile() {
    return profile;
  }

  public void setProfile(Profile profile) {
    this.profile = profile;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Household getHousehold() {
    return household;
  }

  public void setHousehold(Household household) {
    this.household = household;
  }

}
