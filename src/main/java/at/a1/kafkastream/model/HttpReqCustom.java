package at.a1.kafkastream.model;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class HttpReqCustom extends Custom {

  private int latency;
  private String request;
  private String requestMethod;
  private String requestType;
  @JsonProperty("requestURL")
  private String requestUrl;
  private int statusCode;
  private String statusDescription;

  public HttpReqCustom(String concept, int eventTime, String eventId, String event, String linkedSessionId, int latency, String request, String requestMethod, String requestType, String requestUrl, int statusCode, String statusDescription, String eventSource) {
    super(concept, eventTime, eventId, event, linkedSessionId, eventSource);
    this.latency = latency;
    this.request = request;
    this.requestMethod = requestMethod;
    this.requestType = requestType;
    this.requestUrl = requestUrl;
    this.statusCode = statusCode;
    this.statusDescription = statusDescription;
  }

  protected HttpReqCustom() {
  }

  public int getLatency() {
    return latency;
  }

  public void setLatency(int latency) {
    this.latency = latency;
  }

  public String getRequest() {
    return request;
  }

  public void setRequest(String request) {
    this.request = request;
  }

  public String getRequestMethod() {
    return requestMethod;
  }

  public void setRequestMethod(String requestMethod) {
    this.requestMethod = requestMethod;
  }

  public String getRequestType() {
    return requestType;
  }

  public void setRequestType(String requestType) {
    this.requestType = requestType;
  }

  public String getRequestUrl() {
    return requestUrl;
  }

  public void setRequestUrl(String requestUrl) {
    this.requestUrl = requestUrl;
  }

  public int getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(int statusCode) {
    this.statusCode = statusCode;
  }

  public String getStatusDescription() {
    return statusDescription;
  }

  public void setStatusDescription(String statusDescription) {
    this.statusDescription = statusDescription;
  }

}
