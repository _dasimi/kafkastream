package at.a1.kafkastream.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class SessionCustom extends Custom {

  private String sessionType;
  private String sessionId;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "userDataId")
  private UserData userData;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "viewDataId")
  private ViewData viewData;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "applicationDataId")
  private ApplicationData applicationData;

  public SessionCustom(String concept, int eventTime, String eventId, String event, String linkedSessionId, String sessionType, String sessionId, String eventSource, UserData userData, ViewData viewData, ApplicationData applicationData) {
    super(concept, eventTime, eventId, event, linkedSessionId, eventSource);
    this.sessionType = sessionType;
    this.sessionId = sessionId;
    this.userData = userData;
    this.viewData = viewData;
    this.applicationData = applicationData;
  }

  protected SessionCustom() {
  }

  public String getSessionType() {
    return sessionType;
  }

  public void setSessionType(String sessionType) {
    this.sessionType = sessionType;
  }

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public UserData getUserData() {
    return userData;
  }

  public void setUserData(UserData userData) {
    this.userData = userData;
  }

  public ViewData getViewData() {
    return viewData;
  }

  public void setViewData(ViewData viewData) {
    this.viewData = viewData;
  }

  public ApplicationData getApplicationData() {
    return applicationData;
  }

  public void setApplicationData(ApplicationData applicationData) {
    this.applicationData = applicationData;
  }

}
