package at.a1.kafkastream.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Layer {

  @Id
  @JsonProperty("id")
  private String layerId;
  private int vRes;
  private int hRes;
  private int bitrate;

  protected Layer() {
  }

  public Layer(String layerId, int vRes, int hRes, int bitrate) {
    this.layerId = layerId;
    this.vRes = vRes;
    this.hRes = hRes;
    this.bitrate = bitrate;
  }

  public String getLayerId() {
    return layerId;
  }

  public void setLayerId(String layerId) {
    this.layerId = layerId;
  }

  public int getvRes() {
    return vRes;
  }

  public void setvRes(int vRes) {
    this.vRes = vRes;
  }

  public int gethRes() {
    return hRes;
  }

  public void sethRes(int hRes) {
    this.hRes = hRes;
  }

  public int getBitrate() {
    return bitrate;
  }

  public void setBitrate(int bitrate) {
    this.bitrate = bitrate;
  }

}
