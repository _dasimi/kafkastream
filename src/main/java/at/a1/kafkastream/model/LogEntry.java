package at.a1.kafkastream.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Entity
public class LogEntry {

  @Id
  private String logEntryId;

  @JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss.SSSSSS", shape = JsonFormat.Shape.STRING, locale = "en")
  private Date created;

  @JsonProperty("domain_id")
  private int domainId;

  @JsonProperty("household_id")
  private int householdId;

  @JsonProperty("device_id")
  private int deviceId;

  @JsonProperty("user_id")
  private int userId;

  @JsonProperty("profile_id")
  private int profileId;

  private int type;

  @JsonProperty("client_ip")
  private String clientIp;

  @JsonProperty("service_id")
  private int serviceId;

  @JsonProperty("product_id")
  private int productId;

  @JsonProperty("prog_info_id")
  private int progInfoId;

  private int price;

  @JsonProperty("currency_id")
  private int currencyId;
  private Boolean qos;
  @OneToMany(cascade = CascadeType.ALL)
  private List<Custom> customs;
  private String customString;

  protected LogEntry() {
  }

  public LogEntry(String logEntryId, Date created, int domainId, int householdId, int deviceId, int userId, int profileId, int type, String clientIp, Boolean qos, List<Custom> customs, String customString, int serviceId, int productId, int progInfoId, int price, int currencyId) {
    this.logEntryId = logEntryId;
    this.created = created;
    this.domainId = domainId;
    this.householdId = householdId;
    this.deviceId = deviceId;
    this.userId = userId;
    this.profileId = profileId;
    this.type = type;
    this.clientIp = clientIp;
    this.qos = qos;
    this.customs = customs;
    this.customString = customString;
    this.serviceId = serviceId;
    this.productId = productId;
    this.progInfoId = progInfoId;
    this.price = price;
    this.currencyId = currencyId;
  }

  @JsonProperty("custom")
  private void unpackCustom(Object o) {

    if (o instanceof Map) {
      Map<String, Object> custom = (Map<String, Object>) o;
      ObjectMapper mapper = new ObjectMapper();
      this.qos = (Boolean) custom.get("qos");

      if (custom.get("custom") instanceof ArrayList) {
        List<Custom> list = new ArrayList<>();
        for (LinkedHashMap<String, Object> m : (ArrayList<LinkedHashMap<String, Object>>) custom.get("custom")) {

          Custom obj = null;

          if (m.get("concept").equals("HTTPRequest")) {
            obj = mapper.convertValue(m, new TypeReference<HttpReqCustom>() {

            });
          } else if (m.get("concept").equals("Session")) {
            obj = mapper.convertValue(m, new TypeReference<SessionCustom>() {

            });
          } else if (m.get("concept").equals("Issue")) {
            obj = mapper.convertValue(m, new TypeReference<IssueCustom>() {

            });
          } else if (m.get("concept").equals("Playback")) {
            obj = mapper.convertValue(m, new TypeReference<PlaybackCustom>() {

            });
          }
          list.add(obj);

        }
        this.customs = list;
      } else if (custom.get("custom") instanceof String) {
        this.customString = (String) custom.get("custom");
      }
    } else if (o instanceof String) {
      this.customString = (String) o;
    }
  }

  public String getId() {
    return logEntryId;
  }

  public void setId(String logEntryId) {
    this.logEntryId = logEntryId;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public int getDomainId() {
    return domainId;
  }

  public void setDomainId(int domainId) {
    this.domainId = domainId;
  }

  public int getHouseholdId() {
    return householdId;
  }

  public void setHouseholdId(int householdId) {
    this.householdId = householdId;
  }

  public int getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(int deviceId) {
    this.deviceId = deviceId;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public int getProfileId() {
    return profileId;
  }

  public void setProfileId(int profileId) {
    this.profileId = profileId;
  }

  public int getType() {
    return type;
  }

  public void setType(int type) {
    this.type = type;
  }

  public String getClientIp() {
    return clientIp;
  }

  public void setClientIp(String clientIp) {
    this.clientIp = clientIp;
  }

  public Boolean getQos() {
    return qos;
  }

  public void setQos(Boolean qos) {
    this.qos = qos;
  }

  public List<Custom> getCustoms() {
    return customs;
  }

  public void setCustoms(List<Custom> customs) {
    this.customs = customs;
  }

  public String getCustomString() {
    return customString;
  }

  public void setCustomString(String customString) {
    this.customString = customString;
  }

  public int getServiceId() {
    return serviceId;
  }

  public void setServiceId(int serviceId) {
    this.serviceId = serviceId;
  }

  public int getProductId() {
    return productId;
  }

  public void setProductId(int productId) {
    this.productId = productId;
  }

  public int getProgInfoId() {
    return progInfoId;
  }

  public void setProgInfoId(int progInfoId) {
    this.progInfoId = progInfoId;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public int getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(int currencyId) {
    this.currencyId = currencyId;
  }

}

