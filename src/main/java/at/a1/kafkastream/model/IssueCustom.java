package at.a1.kafkastream.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class IssueCustom extends Custom {

  private String code;
  private String message;
  private String severity;
  private String type;
  private String context;
  @Column(length = 1000000)
  private String detail;

  protected IssueCustom() {
  }

  public IssueCustom(String concept, long eventTime, String eventId, String event, String linkedSessionId, String eventSource, String code, String message, String severity, String type, String context, String detail) {
    super(concept, eventTime, eventId, event, linkedSessionId, eventSource);
    this.code = code;
    this.message = message;
    this.severity = severity;
    this.type = type;
    this.context = context;
    this.detail = detail;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getSeverity() {
    return severity;
  }

  public void setSeverity(String severity) {
    this.severity = severity;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getContext() {
    return context;
  }

  public void setContext(String context) {
    this.context = context;
  }

  public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

}
