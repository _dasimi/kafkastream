package at.a1.kafkastream.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Profile {

  @Id
  private String profileId;
  private String profileType;
  private int profileAgeRating;

  public Profile(String profileId, String profileType, int profileAgeRating) {
    this.profileId = profileId;
    this.profileType = profileType;
    this.profileAgeRating = profileAgeRating;
  }

  protected Profile() {
  }

  public String getProfileId() {
    return profileId;
  }

  public void setProfileId(String profileId) {
    this.profileId = profileId;
  }

  public String getProfileType() {
    return profileType;
  }

  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }

  public int getProfileAgeRating() {
    return profileAgeRating;
  }

  public void setProfileAgeRating(int profileAgeRating) {
    this.profileAgeRating = profileAgeRating;
  }

}
