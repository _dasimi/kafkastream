package at.a1.kafkastream.model;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class ContentRef {

  @Id
  private String contentId;
  private String contentType;
  private String channelId;
  private String contentName;
  private String channelName;
  private String originalTitle;
  private String episodeId;
  private String episodeName;
  private String episodeSequenceName;
  private String seasonSequenceName;
  private String seriesId;
  @ElementCollection
  private List<String> genreName;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "viewDataId")
  private ViewData viewData;

  public ContentRef(String contentType, String channelId, String contentId, String contentName, String channelName, String originalTitle, String episodeId, String episodeName, String episodeSequenceName, String seasonSequenceName, String seriesId, List<String> genreName) {
    this.contentType = contentType;
    this.channelId = channelId;
    this.contentId = contentId;
    this.contentName = contentName;
    this.channelName = channelName;
    this.originalTitle = originalTitle;
    this.episodeId = episodeId;
    this.episodeName = episodeName;
    this.episodeSequenceName = episodeSequenceName;
    this.seasonSequenceName = seasonSequenceName;
    this.seriesId = seriesId;
    this.genreName = genreName;
  }

  protected ContentRef() {
  }

  public String getContentType() {
    return contentType;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  public String getChannelId() {
    return channelId;
  }

  public void setChannelId(String channelId) {
    this.channelId = channelId;
  }

  public String getContentId() {
    return contentId;
  }

  public void setContentId(String contentId) {
    this.contentId = contentId;
  }

  public String getContentName() {
    return contentName;
  }

  public void setContentName(String contentName) {
    this.contentName = contentName;
  }

  public String getChannelName() {
    return channelName;
  }

  public void setChannelName(String channelName) {
    this.channelName = channelName;
  }

  public String getOriginalTitle() {
    return originalTitle;
  }

  public void setOriginalTitle(String originalTitle) {
    this.originalTitle = originalTitle;
  }

  public ViewData getViewData() {
    return viewData;
  }

  public void setViewData(ViewData viewData) {
    this.viewData = viewData;
  }

  public String getEpisodeId() {
    return episodeId;
  }

  public void setEpisodeId(String episodeId) {
    this.episodeId = episodeId;
  }

  public String getEpisodeName() {
    return episodeName;
  }

  public void setEpisodeName(String episodeName) {
    this.episodeName = episodeName;
  }

  public String getEpisodeSequenceName() {
    return episodeSequenceName;
  }

  public void setEpisodeSequenceName(String episodeSequenceName) {
    this.episodeSequenceName = episodeSequenceName;
  }

  public String getSeasonSequenceName() {
    return seasonSequenceName;
  }

  public void setSeasonSequenceName(String seasonSequenceName) {
    this.seasonSequenceName = seasonSequenceName;
  }

  public String getSeriesId() {
    return seriesId;
  }

  public void setSeriesId(String seriesId) {
    this.seriesId = seriesId;
  }

  public List<String> getGenreName() {
    return genreName;
  }

  public void setGenreName(List<String> genreName) {
    this.genreName = genreName;
  }

}
