package at.a1.kafkastream.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ApplicationData {

  @Id
  @GeneratedValue
  private int applicationDataId;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "deviceId")
  private Device device;

  public ApplicationData(Device device) {
    this.device = device;
  }

  protected ApplicationData() {
  }

  public int getApplicationDataId() {
    return applicationDataId;
  }

  public void setApplicationDataId(int applicationDataId) {
    this.applicationDataId = applicationDataId;
  }

  public Device getDevice() {
    return device;
  }

  public void setDevice(Device device) {
    this.device = device;
  }

}
