package at.a1.kafkastream.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class PlaybackCustom extends Custom {

  private long delay;
  private int position;
  private int rate;
  private String videoServerSessionId;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "layerId")
  private Layer layer;

  protected PlaybackCustom() {
  }

  public PlaybackCustom(String concept, long eventTime, String eventId, String event, String linkedSessionId, String eventSource, long delay, int position, int rate, String videoServerSessionId) {
    super(concept, eventTime, eventId, event, linkedSessionId, eventSource);
    this.delay = delay;
    this.position = position;
    this.rate = rate;
    this.videoServerSessionId = videoServerSessionId;
  }

  public long getDelay() {
    return delay;
  }

  public void setDelay(long delay) {
    this.delay = delay;
  }

  public int getPosition() {
    return position;
  }

  public void setPosition(int position) {
    this.position = position;
  }

  public int getRate() {
    return rate;
  }

  public void setRate(int rate) {
    this.rate = rate;
  }

  public Layer getLayer() {
    return layer;
  }

  public void setLayer(Layer layer) {
    this.layer = layer;
  }

  public String getVideoServerSessionId() {
    return videoServerSessionId;
  }

  public void setVideoServerSessionId(String videoServerSessionId) {
    this.videoServerSessionId = videoServerSessionId;
  }

}
