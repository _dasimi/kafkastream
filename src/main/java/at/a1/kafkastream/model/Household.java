package at.a1.kafkastream.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Household {

  @Id
  private int householdId;
  private String householdType;

  public Household(int householdId, String householdType) {
    this.householdId = householdId;
    this.householdType = householdType;
  }

  protected Household() {
  }

  public int getHouseholdId() {
    return householdId;
  }

  public void setHouseholdId(int householdId) {
    this.householdId = householdId;
  }

  public String getHouseholdType() {
    return householdType;
  }

  public void setHouseholdType(String householdType) {
    this.householdType = householdType;
  }

}
