package at.a1.kafkastream.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import at.a1.kafkastream.dao.LogEntryDao;
import at.a1.kafkastream.dto.ErrorReportDto;
import at.a1.kafkastream.model.LogEntry;

@Service
public class LogEntryService {

  @Autowired
  LogEntryDao logEntryDao;

  public void create(LogEntry e) {
    logEntryDao.save(e);
  }

  public LogEntry getLogEntryById(String id) {
    return logEntryDao.findById(id).get();
  }

  public List<LogEntry> getLogEntriesByAttributes(Integer householdId, Integer deviceId, Integer userId, Integer profileId, Integer type) {
    return logEntryDao.findLogEntriesByAttributes(householdId, deviceId, userId, profileId, type);
  }

  public List<ErrorReportDto> getErrorsByAttribute(Integer household, Integer device, Integer user, Integer profile, Long from, Long to) {
    return logEntryDao.findErrorsByAttribute(household, device, user, profile, from, to);
  }

  public Map<String, Object> updateLogs(Map<String, String> data) {

    HashMap<String, Object> response = new HashMap<>();
    List<LogEntry> entries;
    if (data != null) {

      entries = logEntryDao.findLogEntriesByAttributes(parseInt(data.get("household")), parseInt(data.get("device")), parseInt(data.get("user")), parseInt(data.get("profile")), parseInt(data.get("type")));

      response.put("user_val", data.get("user"));
      response.put("profile_val", data.get("profile"));
      response.put("household_val", data.get("household"));
      response.put("device_val", data.get("device"));
      response.put("type_val", data.get("type"));
    } else {

      entries = logEntryDao.findAllLogEntries();

      response.put("user_val", "");
      response.put("profile_val", "");
      response.put("household_val", "");
      response.put("device_val", "");
      response.put("type_val", "");
    }

    response.put("user", entries.stream().map(e -> e.getUserId()).distinct().sorted().collect(Collectors.toList()));
    response.put("profile", entries.stream().map(e -> e.getProfileId()).distinct().sorted().collect(Collectors.toList()));
    response.put("household", entries.stream().map(e -> e.getHouseholdId()).distinct().sorted().collect(Collectors.toList()));
    response.put("device", entries.stream().map(e -> e.getDeviceId()).distinct().sorted().collect(Collectors.toList()));
    response.put("type", entries.stream().map(e -> e.getType()).distinct().sorted().collect(Collectors.toList()));

    if (data != null) {
      if (data.size() != 0) {
        for (String name : new String[] { "user", "profile", "household", "device", "type" }) {

          if (numberOfEmptyValues(data) >= 4 && !data.get(name).equals("")) {
            entries = logEntryDao.findAllLogEntries();

            response.put(name, entries.stream()
                .map(e -> {
                  try {
                    return e.getClass().getMethod("get" + capitalize(name) + (name.equals("type") ? "" : "Id")).invoke(e);
                  } catch (Exception exc) {
                    return "";
                  }
                }).distinct().sorted().collect(Collectors.toList()));
          }
        }
      }
    }

    return response;
  }

  public Map<String, Object> updateErrors(Map<String, String> data) {

    HashMap<String, Object> response = new HashMap<>();
    List<ErrorReportDto> entries;

    if (data != null) {

      entries = logEntryDao.findErrorsByAttribute(parseInt(data.get("household")), parseInt(data.get("device")), parseInt(data.get("user")), parseInt(data.get("profile")), null, null);

      response.put("user_val", data.get("user"));
      response.put("profile_val", data.get("profile"));
      response.put("household_val", data.get("household"));
      response.put("device_val", data.get("device"));
    } else {
      entries = logEntryDao.findAllErrors();

      response.put("user_val", "");
      response.put("profile_val", "");
      response.put("household_val", "");
      response.put("device_val", "");
    }
    response.put("user", entries.stream()
        .map(ErrorReportDto::getUserId)
        .distinct()
        .sorted()
        .collect(Collectors.toList()));
    response.put("profile", entries.stream().map(e -> e.getProfileId()).distinct().sorted().collect(Collectors.toList()));
    response.put("household", entries.stream().map(e -> e.getHouseholdId()).distinct().sorted().collect(Collectors.toList()));
    response.put("device", entries.stream().map(e -> e.getDeviceId()).distinct().sorted().collect(Collectors.toList()));

    if (data != null) {
      if (data.size() != 0) {
        for (String name : new String[] { "user", "profile", "household", "device" }) {

          if (numberOfEmptyValues(data) >= 3 && !data.get(name).equals("")) {
            entries = logEntryDao.findAllErrors();

            response.put(name, entries.stream()
                .map(e -> {
                  try {
                    return e.getClass().getMethod("get" + capitalize(name) + "Id").invoke(e);
                  } catch (Exception exc) {
                    return "";
                  }
                }).distinct().sorted().collect(Collectors.toList()));
          }
        }
      }
    }

    return response;
  }

  public List<Object> getErrorsByHousehold() {
    return logEntryDao.getErrorsByHousehold();
  }

  public void fillDB() {
    System.out.println("adding data events log");
    for (LogEntry logEntry : getListFromFile("26_22-27_06.log")) {
      create(logEntry);
    }
  }

  private List<LogEntry> getListFromFile(String filename) {

    try {
      File file = ResourceUtils.getFile("classpath:\\kafkaLogs\\" + filename);
      BufferedReader reader = new BufferedReader(new FileReader(file));

      String jsonString = "[";
      String content;
      while ((content = reader.readLine()) != null) {
        jsonString += content.equals("}") ? "}," : content;
      }
      jsonString += "]";

      jsonString = jsonString.replace("\\", "").replace("\"{", "{").replace("}\"", "}").replace("\"[{", "[{").replace("}]\"", "}]").replace(",]", "]");
      ObjectMapper mapper = new ObjectMapper();

      JsonNode node = mapper.readValue(jsonString, new TypeReference<JsonNode>() {

      });

      List<LogEntry> list = new ArrayList<>();
      for (int i = 0; i < node.size(); i++) {
        JsonNode entryJson = node.get(i);
        list.add(mapper.convertValue(entryJson, new TypeReference<LogEntry>() {

        }));
      }
      return list;
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }

    return null;
  }

  private Integer parseInt(String string) {
    try {
      return string.equals("") || string.equals("all Values") ? null : Integer.parseInt(string);
    } catch (Exception e) {
      return null;
    }
  }

  private int numberOfEmptyValues(Map<String, String> data) {
    int counter = 0;

    if (data.get("user").equals("")) {
      counter++;
    }
    if (data.get("profile").equals("")) {
      counter++;
    }
    if (data.get("household").equals("")) {
      counter++;
    }
    if (data.get("device").equals("")) {
      counter++;
    }
    if (data.containsKey("type")) {
      if (data.get("type").equals("")) {
        counter++;
      }
    }
    return counter;
  }

  private String capitalize(String string) {
    return string.substring(0, 1).toUpperCase() + string.substring(1);
    // StringUtils.capitalize();
  }

}
