package at.a1.kafkastream.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorReportDto {

  private String logEntryId;
  private int userId;
  private int profileId;
  private int deviceId;
  private int householdId;
  private String concept;
  private long eventTime;
  private String requestMethod;
  private String request;
  private String requestUrl;
  private Integer statusCode;
  private String statusDescription;
  private String code;
  private String context;
  private String severity;
  private String message;
  private String detail;
  private String type;

  public ErrorReportDto(String logEntryId, int userId, int profileId, int deviceId, int householdId, String concept, long eventTime, String requestMethod, String request, String requestUrl, Integer statusCode, String statusDescription, String code, String context, String severity, String message, String detail, String type) {
    this.logEntryId = logEntryId;
    this.userId = userId;
    this.profileId = profileId;
    this.deviceId = deviceId;
    this.householdId = householdId;
    this.concept = concept;
    this.eventTime = eventTime;
    this.requestMethod = requestMethod;
    this.request = request;
    this.requestUrl = requestUrl;
    this.statusCode = statusCode;
    this.statusDescription = statusDescription;
    this.code = code;
    this.context = context;
    this.severity = severity;
    this.message = message;
    this.detail = detail;
    this.type = type;
  }

  public String getLogEntryId() {
    return logEntryId;
  }

  public void setLogEntryId(String logEntryid) {
    this.logEntryId = logEntryid;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public int getProfileId() {
    return profileId;
  }

  public void setProfileId(int profileId) {
    this.profileId = profileId;
  }

  public int getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(int deviceId) {
    this.deviceId = deviceId;
  }

  public int getHouseholdId() {
    return householdId;
  }

  public void setHouseholdId(int householdId) {
    this.householdId = householdId;
  }

  public String getConcept() {
    return concept;
  }

  public void setConcept(String concept) {
    this.concept = concept;
  }

  public long getEventTime() {
    return eventTime;
  }

  public void setEventTime(long eventTime) {
    this.eventTime = eventTime;
  }

  public String getRequestMethod() {
    return requestMethod;
  }

  public void setRequestMethod(String requestMethod) {
    this.requestMethod = requestMethod;
  }

  public String getRequest() {
    return request;
  }

  public void setRequest(String request) {
    this.request = request;
  }

  public String getRequestUrl() {
    return requestUrl;
  }

  public void setRequestUrl(String requestUrl) {
    this.requestUrl = requestUrl;
  }

  public Integer getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(Integer statusCode) {
    this.statusCode = statusCode;
  }

  public String getStatusDescription() {
    return statusDescription;
  }

  public void setStatusDescription(String statusDescription) {
    this.statusDescription = statusDescription;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getContext() {
    return context;
  }

  public void setContext(String context) {
    this.context = context;
  }

  public String getSeverity() {
    return severity;
  }

  public void setSeverity(String severity) {
    this.severity = severity;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

}
