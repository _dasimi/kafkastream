package at.a1.kafkastream.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import at.a1.kafkastream.service.LogEntryService;

@Controller
public class AppController {

  @Autowired
  private LogEntryService logEntryService;

  @PostMapping(value = "/updateLogs")
  @ResponseBody
  public Map<String, Object> updateLogsAction(@RequestParam HashMap<String, String> data) {
    return logEntryService.updateLogs(data);
  }

  @PostMapping(value = "/updateErrors")
  @ResponseBody
  public Map<String, Object> updateErrorsAction(@RequestParam HashMap<String, String> data) {
    return logEntryService.updateErrors(data);
  }

  @RequestMapping(value = "/")
  public String index(Model model) {
    Map<String, Object> logData = logEntryService.updateLogs(null);
    model.addAttribute("logUsers", logData.get("user"));
    model.addAttribute("logProfiles", logData.get("profile"));
    model.addAttribute("logHouseholds", logData.get("household"));
    model.addAttribute("logDevices", logData.get("device"));
    model.addAttribute("logTypes", logData.get("type"));

    Map<String, Object> errorData = logEntryService.updateErrors(null);
    model.addAttribute("errorUsers", errorData.get("user"));
    model.addAttribute("errorProfiles", errorData.get("profile"));
    model.addAttribute("errorHouseholds", errorData.get("household"));
    model.addAttribute("errorDevices", errorData.get("device"));

    return "index";
  }

}
