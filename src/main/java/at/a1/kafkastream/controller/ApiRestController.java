package at.a1.kafkastream.controller;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import at.a1.kafkastream.dto.ErrorReportDto;
import at.a1.kafkastream.model.LogEntry;
import at.a1.kafkastream.service.LogEntryService;

@RestController
public class ApiRestController {

  @Autowired
  LogEntryService logEntryService;

  @RequestMapping(value = "/fillDb")
  public void fillDb() {
    logEntryService.fillDB();
    System.out.println("DATABASE FILLED!");
  }

  @RequestMapping("/logs")
  public List<LogEntry> getLogEntriesByAttribute(@RequestParam(required = false) Integer user, @RequestParam(required = false) Integer profile, @RequestParam(required = false) Integer household, @RequestParam(required = false) Integer device, @RequestParam(required = false) Integer type) {
    return logEntryService.getLogEntriesByAttributes(household, device, user, profile, type);
  }

  @RequestMapping(value = "logs/{id}")
  public LogEntry getLogEntryById(@PathVariable String id) {
    return logEntryService.getLogEntryById(id);
  }

  @RequestMapping(value = "/errorReport")
  public List<ErrorReportDto> getErrorEntries(@RequestParam(required = false) Integer user, @RequestParam(required = false) Integer profile, @RequestParam(required = false) Integer household, @RequestParam(required = false) Integer device, @RequestParam(required = false) String from, @RequestParam(required = false) String to, @RequestParam(required = false) String id) {

    Long fromTimestamp;
    Long toTimestamp;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

    try {
      fromTimestamp = from != null ? simpleDateFormat.parse(from).getTime() : null;
    } catch (Exception e) {
      fromTimestamp = null;
    }
    try {
      toTimestamp = from != null ? simpleDateFormat.parse(to).getTime() : null;
    } catch (Exception e) {
      toTimestamp = null;
    }
    return logEntryService.getErrorsByAttribute(household, device, user, profile, fromTimestamp, toTimestamp);
  }

  @RequestMapping(value = "/errorsByHousehold")
  public List<Object> getErrorsByHousehold() {
    return logEntryService.getErrorsByHousehold();
  }

}
