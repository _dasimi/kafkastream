package at.a1.kafkastream.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import at.a1.kafkastream.dto.ErrorReportDto;
import at.a1.kafkastream.model.LogEntry;

@Repository
public interface LogEntryDao extends CrudRepository<LogEntry, String> {

  @Query("select e from LogEntry e")
  List<LogEntry> findAllLogEntries();

  @Query("select e from LogEntry e where (?1 = null or e.householdId = ?1) and (?2 = null or e.deviceId = ?2) and (?3 = null or e.userId = ?3) and (?4 = null or e.profileId = ?4) and (?5 = null or e.type = ?5)")
  public List<LogEntry> findLogEntriesByAttributes(Integer householdId, Integer deviceId, Integer userId, Integer profileId, Integer type);

  @Query("select new at.a1.kafkastream.dto.ErrorReportDto(e.logEntryId, e.userId, e.profileId, e.deviceId, e.householdId, c.concept, c.eventTime, c.requestMethod, c.requestType, c.requestUrl, c.statusCode, c.statusDescription, c.code, c.context, c.severity, c.message, c.detail, c.type) from LogEntry e join e.customs c where (c.severity='ERROR' or c.statusCode !=200) order by c.eventTime")
  public List<ErrorReportDto> findAllErrors();

  @Query("select new at.a1.kafkastream.dto.ErrorReportDto(e.logEntryId, e.userId, e.profileId, e.deviceId, e.householdId, c.concept, c.eventTime, c.requestMethod, c.requestType, c.requestUrl, c.statusCode, c.statusDescription, c.code, c.context, c.severity, c.message, c.detail, c.type) from LogEntry e join e.customs c where (c.severity='ERROR' or c.statusCode !=200) and (?1 = null or e.householdId = ?1) and (?2 = null or e.deviceId = ?2) and (?3 = null or e.userId = ?3) and (?4 = null or e.profileId = ?4) and (?5 = null or c.eventTime >= ?5) and (?6 = null or c.eventTime <= ?6) order by c.eventTime")
  public List<ErrorReportDto> findErrorsByAttribute(Integer householdId, Integer deviceId, Integer userId, Integer profileId, Long from, Long to);

  @Query("select e.householdId, count(*) from LogEntry e join e.customs c where (c.severity='ERROR' or c.statusCode !=200) group by e.householdId")
  public List<Object> getErrorsByHousehold();

}
