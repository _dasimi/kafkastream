var programmatically = false;
$(document).ready(() => {

    $('.selectList').on('select', changed => {
        if(!programmatically){
            $(changed.target).jqxDropDownList('unselectItem','all');
            setDropDownContent(getData(changed));
        }
    });
});

function addEmptyValue(obj) {
    obj.push("all");
    return obj
}
function setDropDownContent(data) {
    $.post('/update'+(group.charAt(0).toUpperCase() + group.slice(1)), data, res => {
        programmatically=true;
        $('#user_' + group + '_ac').jqxDropDownList(getJqxDropDownListParams(addEmptyValue(res.user)));
        $('#user_' + group + '_ac').jqxDropDownList('selectItem', res.user_val);
        $('#profile_' + group + '_ac').jqxDropDownList(getJqxDropDownListParams(addEmptyValue(res.profile)));
        $('#profile_' + group + '_ac').jqxDropDownList('selectItem', res.profile_val);
        $('#household_' + group + '_ac').jqxDropDownList(getJqxDropDownListParams(addEmptyValue(res.household)));
        $('#household_' + group + '_ac').jqxDropDownList('selectItem', res.household_val);
        $('#device_' + group + '_ac').jqxDropDownList(getJqxDropDownListParams(addEmptyValue(res.device)));
        $('#device_' + group + '_ac').jqxDropDownList('selectItem', res.device_val);

        if(group=='logs') {
            $('#type_logs_ac').jqxDropDownList(getJqxDropDownListParams(addEmptyValue(res.type)));
            $('#type_logs_ac').jqxDropDownList('selectItem', res.type_val);
        }

        programmatically=false;
    });
}
function getData(changed) {
    var data = {};

    data[$(changed.target).attr("name")] = $(changed.target).val()=='all'?null:$(changed.target).val();
    $(changed.target).siblings(".selectList").each((i, e) => {
        data[$(e).attr("name")]= $(e).jqxDropDownList('getSelectedItem') == null ? null : $(e).jqxDropDownList('getSelectedItem').value;
    });
    return data;
}
function compareUpdates(u1, u2) {
    if(u1 != null && u2 != null) {
        return u1.user==u2.user && u1.profile==u2.profile && u1.household==u2.household && u1.device==u2.device && u1.type==u2.type;
    }
    return false;
}
function getJqxDropDownListParams(source) {
     return {source: source, theme: 'metro', autoDropDownHeight: source.length>8?false:true, };

 }