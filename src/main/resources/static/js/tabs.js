var group = "logs";
$(document).ready(function() {

    $('#logs_btn').click(function() {
        $('#logs_btn').addClass('active');
        $('#errors_btn').removeClass('active');
        $('#logs_content').addClass('active');
        $('#errors_content').removeClass('active');
        group = "logs";
    });
    $('#errors_btn').click(function() {
        $('#errors_btn').addClass('active');
        $('#logs_btn').removeClass('active');
        $('#errors_content').addClass('active');
        $('#logs_content').removeClass('active');
        group="errors"
    });
});